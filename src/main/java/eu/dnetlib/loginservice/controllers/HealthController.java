package eu.dnetlib.loginservice.controllers;

import eu.dnetlib.loginservice.configuration.GlobalVars;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HealthController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final GlobalVars globalVars;

    @Autowired
    public HealthController(GlobalVars globalVars) {
        this.globalVars = globalVars;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from Login service!");
        return "Hello from Login service!";
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();
        if(GlobalVars.date != null) {
            response.put("Date of deploy", eu.dnetlib.authentication.configuration.GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}
