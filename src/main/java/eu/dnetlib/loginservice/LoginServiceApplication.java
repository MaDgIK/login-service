package eu.dnetlib.loginservice;

import eu.dnetlib.authentication.configuration.AuthenticationConfiguration;
import eu.dnetlib.loginservice.configuration.APIProperties;
import eu.dnetlib.loginservice.configuration.GlobalVars;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.loginservice"})
@PropertySources({
        @PropertySource("classpath:authentication.properties"),
        @PropertySource("classpath:login-service.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@Import({AuthenticationConfiguration.class})
@EnableConfigurationProperties({APIProperties.class, GlobalVars.class})
public class LoginServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginServiceApplication.class, args);
    }

}
