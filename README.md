# Login Service

Login service provides methods to authenticate users through AAI provider
and retrieve information of authenticated user.

## Configuration

#### AAI Configuration

    authentication.oidc.issuer=https://aai.openaire.eu/oidc/
    authentication.oidc.logout=https://aai.openaire.eu/proxy/saml2/idp/SingleLogoutService.php?ReturnTo=
    authentication.oidc.home=https:://<domain>/login-service/openid_connect_login
    authentication.oidc.scope=openid,profile,email,eduperson_entitlement
    authentication.oidc.id=id
    authentication.oidc.secret=secret

#### Cookie - Session

    authentication.session=openAIRESession
    authentication.domain=di.uoa.gr

#### Redis Properties

    authentication.redis.host = localhost
    authentication.redis.port = 6379
    authentication.redis.password =

#### Default Redirect

    authentication.redirect=<default-redirect-URI>


## Methods

### Login

    https://<domain>/login-service/openid_connect_login?redirect=<redirectURI>

### Logout

    https://<domain>/login-service/openid_logout?redirect=<redirectURI>

### User Info

    https://<domain>/login-service/userInfo

e.g
    
    {
        "sub":"123456789012345@openaire.eu",
        "name":"John Doe",
        "given_name":"John",
        "family_name":"Doe",
        "email":"john.doe@example.com",
        "roles":[
            "REGISTERED_USER",
            "PORTAL_ADMINISTRATOR"
        ]
    }
    
